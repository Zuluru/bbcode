<?php

use Cake\Core\Configure;

if (!Configure::check('BBCode.renderCell')) {
    Configure::write('BBCode.renderCell', 'BBCode.TagRenderer');
}