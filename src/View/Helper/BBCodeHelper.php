<?php

namespace BBCode\View\Helper;

use BBCode\Lib\BBCodeParser;
use Cake\Core\Configure;
use Cake\View\Helper;

/**
 * a view helper to render BBCode in views
 *
 * @package BBCode\View\Helper
 */
class BBCodeHelper extends Helper {

    /**
     * @var BBCodeParser
     */
    private $_parser;

    public function initialize (array $config) {
        parent::initialize($config);
        $this->_parser = new BBCodeParser(Configure::check('debug'));
    }

    /**
     * renders the given BBCode string to HTML
     *
     * @param string $bbCode the BBCode to render
     * @return string the rendered HTML
     */
    public function render ($bbCode) {
        return $this->_parser->parse($bbCode);
    }

}