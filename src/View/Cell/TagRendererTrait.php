<?php

namespace BBCode\View\Cell;

use BBCode\Exception\ContentNotAllowedException;
use BBCode\Exception\InvalidAttributeException;
use BBCode\Exception\InvalidParentException;
use BBCode\Exception\MissingAttributeException;
use BBCode\Lib\TagData;

trait TagRendererTrait {

    public abstract function set($name, $value = null);

    /**
     * passes the content of the tag into view var content
     *
     * @param TagData $data the tag data
     */
    protected function _passContent (TagData $data) {
        $this->set('content', $data->content);
    }

    /**
     * asserts that the content is empty.
     *
     * can be used to ensure standalone tags not to have content
     *
     * @param TagData $data the tag data
     * @throws ContentNotAllowedException if the content is not empty
     */
    protected function _assertStandalone (TagData $data) {
        if (strlen($data->content)) {
            throw new ContentNotAllowedException($data->tagName);
        }
    }

    /**
     * checks if the tags parent is valid.
     *
     * can be used to ensure correct tag nesting
     *
     * @param TagData $data the tag data
     * @param array $valid the white-/blacklist to check the parent against
     * @param bool $whiteList false, if $valid is a blacklist, true otherwise
     * @throws InvalidParentException if the tag parents doesn't match the white-/blacklist condition
     */
    protected function _validateParent (TagData $data, array $valid, $whiteList = true) {
        if (in_array($data->parent, $valid) xor $whiteList) {
            throw new InvalidParentException([$data->tagName, $data->parent]);
        }
    }

    /**
     * checks if a given tag attribute is valid.
     *
     * @param TagData $data the tag data
     * @param string $attribute the attribute name
     * @param array $valid the white-/blacklist to check the attribute value against
     * @param bool|mixed $default the default value or false if attribute is required
     * @param bool $whiteList false, if $valid is a blacklist, true otherwise
     */
    protected function _validateAttribute (TagData $data, $attribute, array $valid, $default = false, $whiteList = true) {
        if (!isset($data->attributes->{$attribute})) {
            if ($default === false) {
                throw new MissingAttributeException([h($attribute), $data->tagName]);
            }
            $data->attributes->{$attribute} = $default;
        }
        if (in_array(strtolower($data->attributes->{$attribute}), $valid) xor $whiteList) {
            if ($whiteList) {
                $message = __d('BBCode', 'value must be one of the following');
            } else {
                $message = __d('BBCode', 'value must not be one of the following');
            }
            throw new InvalidAttributeException([h($attribute), $data->tagName, $message . ': ' . implode(', ', $valid)]);
        }
    }

    /**
     * asserts that the required attribute is present.
     *
     * @param TagData $data the tag data
     * @param string $attribute the attribute to check
     * @throws MissingAttributeException if the attribute is not present
     */
    protected function _assertRequired (TagData $data, $attribute) {
        if (!isset($data->attributes->{$attribute})) {
            throw new MissingAttributeException([h($attribute), $data->tagName]);
        }
    }

}