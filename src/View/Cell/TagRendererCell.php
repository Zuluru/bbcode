<?php

namespace BBCode\View\Cell;

use BBCode\Exception\InvalidAttributeException;
use BBCode\Exception\MissingAttributeException;
use BBCode\Exception\TagRenderException;
use BBCode\Lib\TagData;
use Cake\View\Cell;

/**
 * the default implementation of an tag renderer cell defining a basic set of tags
 *
 * @package BBCode\View\Cell
 */
class TagRendererCell extends Cell {

    use TagRendererTrait;

    /**
     * a regex pattern used to validate email addresses against
     */
    const EMAIL_PATTERN = '/^[\p{L}0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[\p{L}0-9!#$%&\'*+\/=?^_`{|}~-]+)*@(?:[_\p{L}0-9][-_\p{L}0-9]*\.)*(?:[\p{L}0-9][-\p{L}0-9]{0,62})\.(?:(?:[a-z]{2}\.)?[a-z]{2,})/ui';

    /**
     * @var array the internal list of inline tags
     */
    static private $_inlineTags = ['b', 'i', 'u', 'sub', 'sup', 'br', 'a', 'email'];

    /**
     * @var array the internal list of of valid alignment values
     */
    static private $_validAlignment = ['left', 'center', 'right', 'justify'];

    /**
     * @var array the internal list of valid list types
     */
    static private $_validListTypes = ['none', 'square', 'circle', 'disc', 'decimal', 'lower-latin', 'upper-latin', 'lower-alpha', 'upper-alpha', 'lower-roman', 'upper-roman',
                                       'lower-greek', 'hebrew', 'decimal-leading-zero', 'cjk-ideographic', 'hiragana', 'katakana', 'hiragana-iroha', 'katakana-iroha', 'armenian',
                                       'georgian'];

    /**
     * @var array the internal list of list types associated with an unordered list
     */
    static private $_ulTypes = ['none', 'square', 'circle', 'disc'];

    /**
     * @var array internal list of list type shortcut conversions
     */
    static private $_transformTypes = ['1' => 'decimal', 'a' => 'lower-latin', 'A' => 'upper-latin', 'i' => 'lower-roman', 'I' => 'upper-roman'];

    /**
     * renders the given content as bold
     *
     * @param TagData $data the tag data
     */
    public function b (TagData $data) {
        $this->_passContent($data);
    }

    /**
     * renders the given content as italic
     *
     * @param TagData $data the tag data
     */
    public function i (TagData $data) {
        $this->_passContent($data);
    }

    /**
     * renders the given content as underlined
     *
     * @param TagData $data the tag data
     */
    public function u (TagData $data) {
        $this->_passContent($data);
    }

    /**
     * renders an HTML line break
     *
     * @param TagData $data the tag data
     */
    public function br (TagData $data) {
        $this->_assertStandalone($data);
    }

    /**
     * renders the given content as subscript
     *
     * @param TagData $data the tag data
     */
    public function sub (TagData $data) {
        $this->_passContent($data);
    }

    /**
     * renders the given content as superscript
     *
     * @param TagData $data the tag data
     */
    public function sup (TagData $data) {
        $this->_passContent($data);
    }

    /**
     * renders either a document anchor or a link tag
     *
     * @param TagData $data the tag data
     */
    public function a (TagData $data) {
        if (isset($data->attributes->name)) {
            $this->template = 'a_name';
            $this->set('name', $data->attributes->name);
        } elseif (isset($data->attributes->url)) {
            $this->set('url', $data->attributes->url);
        } else {
            throw new MissingAttributeException([['name', 'url'], 'a']);
        }
        $this->_passContent($data);
    }

    /**
     * renders an email link tag
     *
     * @param TagData $data the tag data
     */
    public function email (TagData $data) {
        if (!preg_match(self::EMAIL_PATTERN, $data->content)) {
            throw new TagRenderException(__d('BBCode', 'Invalid Email address in tag [email]'));
        }
        $this->set('email', $data->content);
    }

    /**
     * renders the content inside a paragraph.
     *
     * supports alignment and indentation
     *
     * @param TagData $data the tag data
     */
    public function p (TagData $data) {
        $this->_validateParent($data, self::$_inlineTags, false);
        $this->_validateAttribute($data, 'align', self::$_validAlignment, 'left');
        $data->attributes->indent = @$data->attributes->indent ?: 0;
        if (preg_match('/^(\\d+(?:\\.\\d+)?)(px|pt|em|rem|%)?$/', $data->attributes->indent, $matches)) {
            if (empty($matches[2])) {
                $data->attributes->indent = $matches[1] . 'rem';
            }
        } else {
            throw new InvalidAttributeException(['indent', 'p', __d('BBCode', 'value must be a decimal optional followed by one of the following units: px, pt, em, rem, %')]);
        }
        $this->set('style', ['text-align' => $data->attributes->align, 'margin-left' => $data->attributes->indent]);
        $this->_passContent($data);
    }

    /**
     * renders a header tag.
     *
     * supports header levels from 2 to 6
     *
     * @param TagData $data
     */
    public function h (TagData $data) {
        $this->_validateParent($data, self::$_inlineTags, false);
        if (!in_array($data->attributes->level, [2, 3, 4, 5, 6])) {
            throw new InvalidAttributeException(['level', 'h', __d('BBCode', 'value must be an integer between 2 and 6 inclusive')]);
        }
        $this->set('tag', 'h' . $data->attributes->level);
        $this->_passContent($data);
    }

    /**
     * renders an HTML table.
     *
     * supports alignment
     *
     * @param TagData $data the tag data
     */
    public function table (TagData $data) {
        $this->_validateParent($data, self::$_inlineTags, false);
        $this->_validateAttribute($data, 'align', self::$_validAlignment, 'left');
        $this->set('align', $data->attributes->align);
        $this->_passContent($data);
    }

    /**
     * renders a table row
     *
     * @param TagData $data the tag row
     */
    public function tr (TagData $data) {
        $this->_validateParent($data, ['table']);
        $this->_passContent($data);
    }

    /**
     * renders a table header cell
     *
     * @param TagData $data the tag data
     */
    public function th (TagData $data) {
        $this->_validateParent($data, ['tr']);
        $this->_passContent($data);
    }

    /**
     * renders a table cell
     *
     * @param TagData $data the tag data
     */
    public function td (TagData $data) {
        $this->_validateParent($data, ['tr']);
        $this->_passContent($data);
    }

    /**
     * renders an (un-)ordered list.
     *
     * content must be json array like, supports all valid css list style types and start number for ordered lists
     *
     * @param TagData $data the tag data
     */
    public function _list (TagData $data) {
        $this->_validateParent($data, self::$_inlineTags, false);
        if (array_key_exists($data->attributes->type, self::$_transformTypes)) {
            $data->attributes->type = self::$_transformTypes;
        }
        $this->_validateAttribute($data, 'type', self::$_validListTypes, 'none');
        $start = isset($data->attributes->start);
        if (in_array($data->attributes->type, self::$_ulTypes)) {
            $this->template = 'ul';
            if ($start) {
                throw new InvalidAttributeException(['start', 'list', sprintf(__d('BBCode', 'attribute not allowed for type="%s"'), $data->attributes->type)]);
            }
        } else {
            $this->template = 'ol';
            if ($start) {
                $this->set('start', $data->attributes->start);
            }
        }
        $this->set('list', json_decode(trim($data->content, '[]')));
    }

    /**
     * renders a definition list.
     *
     * content must be json object like
     *
     * @param TagData $data the tag data
     */
    public function dl (TagData $data) {
        $this->_validateParent($data, self::$_inlineTags, false);
        $this->set('definitions', json_decode(trim($data->content, '{}')));
    }

}