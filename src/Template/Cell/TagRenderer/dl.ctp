<dl>
    <?php foreach ($definitions as $term => $definition): ?>
    <dt><?= $term ?></dt>
    <dd><?= $definition ?></dd>
    <?php endforeach; ?>
</dl>