<?php

namespace BBCode\Validation;

use BBCode\Lib\BBCodeParser;
use Cake\Core\Exception\Exception;

/**
 * a validation provider class to allow syntax checking of BBCode fields
 *
 * @package BBCode\Validation
 */
class BBCodeValidator {

    /**
     * validates a given BBCode strings syntax
     *
     * @param string $check the BBCode to check
     * @return bool|string true if the string is valid, the error message otherwise
     */
    public static function validate ($check) {
        try {
            (new BBCodeParser(true))->parse($check);
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

}