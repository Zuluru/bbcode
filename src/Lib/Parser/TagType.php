<?php

namespace BBCode\Lib\Parser;

/**
 * (enum) class to represent the valid tag types
 *
 * @package BBCode\Lib\Parser
 */
class TagType {

    const StartTag = 0;
    const EndTag = 1;
    const StandaloneTag = 2;

}