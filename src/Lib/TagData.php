<?php

namespace BBCode\Lib;

/**
 * a class representing all relevant information to render a tag
 *
 * @package BBCode
 * @property-read string tagName the tag name
 * @property-read \stdClass attributes the attributes as object
 * @property-read string content the tag content
 * @property-read string parent the name of the parent tag or an empty string if no parent is present
 */
class TagData implements \JsonSerializable {

    private $_data;

    public function __construct ($tagName, array $attributes, $content, $parent) {
        $attributes = (object) $attributes;
        $this->_data = compact('tagName', 'attributes', 'content', 'parent');
    }

    public function __get ($name) {
        return @$this->_data[$name];
    }

    function jsonSerialize () {
        return $this->_data;
    }
}