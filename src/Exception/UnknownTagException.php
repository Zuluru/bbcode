<?php

namespace BBCode\Exception;

/**
 * an exception to report not implemented tags
 *
 * @package BBCode\Exception
 */
class UnknownTagException extends TagRenderException {

    public function __construct ($message, $code = 500, $previous = null) {
        $this->_messageTemplate = __d('BBCode', 'unknown tag [%s]');
        parent::__construct($message, $code, $previous);
    }

}