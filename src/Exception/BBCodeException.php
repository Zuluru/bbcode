<?php

namespace BBCode\Exception;

use Cake\Core\Exception\Exception;

/**
 * the base exception class for structural syntax errors
 *
 * @package BBCode\Exception
 */
class BBCodeException extends Exception {

    protected $_message = '';

    /**
     * BBCodeException constructor.
     *
     * @param array|string $message the message to display or the attributes if $_message is set in subclass
     * @param int $position the error position
     * @param string $bbCode the original BBCode string
     */
    public function __construct ($message, $position, $bbCode) {
        if (!empty($this->_message)) {
            $message = vsprintf($this->_message, (array) $message);
        }
        $this->_messageTemplate = __d('BBCode', "BBCodeParserError: %s at char #%u near '%s'");
        parent::__construct([$message, $position, preg_replace('/^(?:\S*\s+)(.*)(?:\s+\S*)$/is', '\1', substr($bbCode, min(0, $position - 30), 100))]);
    }

}