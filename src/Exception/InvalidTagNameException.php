<?php

namespace BBCode\Exception;

/**
 * an exception class to report tag names containing invalid chars
 *
 * @package BBCode\Exception
 */
class InvalidTagNameException extends BBCodeException {

    public function __construct ($message, $position, $bbCode) {
        $this->_message = __d('BBCode', "Tagname '%s' is invalid");
        parent::__construct($message, $position, $bbCode);
    }

}