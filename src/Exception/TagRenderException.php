<?php

namespace BBCode\Exception;

use Cake\Core\Exception\Exception;

/**
 * the basic exception class used for all tag render errors.
 *
 * a custom error message can be passed
 *
 * @package BBCode\Exception
 */
class TagRenderException extends Exception {

    protected $_messageTemplate = '%s';

    public function __construct ($message, $code = 500, $previous = null) {
        $this->_messageTemplate = __d('BBCode', 'RenderTagError') . ': ' . $this->_messageTemplate;
        parent::__construct((array) $message, $code, $previous);
    }

}