<?php

namespace BBCode\Exception;

/**
 * an exception class to report all syntax errors caused by unexpected chars
 *
 * @package BBCode\Exception
 */
class UnexpectedStringException extends BBCodeException {

    public function __construct ($message, $position, $bbCode, $escape1 = "'%1\$s'", $escape2 = "'%2\$s'") {
        switch (count((array) $message)) {
            case 1:
                $this->_message = __d('BBCode', 'Unexpected %1$s');
                break;
            case 2:
                $this->_message = __d('BBCode', 'Expected %1$s, got %2$s');
        }
        if ($escape1) {
            $this->_message = str_replace('%1$s', $escape1, $this->_message);
        }
        if ($escape2) {
            $this->_message = str_replace('%2$s', $escape2, $this->_message);
        }
        parent::__construct($message, $position, $bbCode);
    }

}