<?php

require_once 'vendor/autoload.php';

use Cake\Cache\Cache;
use Cake\Core\Configure;

define('ROOT', dirname(__DIR__));

define('CAKE_CORE_INCLUDE_PATH', ROOT . '/vendor/cakephp/cakephp');
define('CORE_PATH', CAKE_CORE_INCLUDE_PATH . DS);
define('CAKE', CORE_PATH . 'src' . DS);
define('CORE_TESTS', CORE_PATH . 'tests' . DS);
define('CORE_TEST_CASES', CORE_TESTS . 'TestCase');
define('TMP', sys_get_temp_dir() . DS);
define('CACHE', TMP . 'cache' . DS);
define('APP', ROOT . DS . 'src' . DS);

@mkdir(CACHE);

require_once CORE_PATH . 'config' . DS . 'bootstrap.php';

date_default_timezone_set('UTC');
mb_internal_encoding('UTF-8');

Configure::write('debug', true);
Configure::write('App.paths.templates', [ROOT . '/tests/TestCase/']);

Cache::config([
                  '_cake_core_' => [
                      'engine' => 'File',
                      'prefix' => 'cake_core_',
                      'serialize' => true
                  ],
                  '_cake_model_' => [
                      'engine' => 'File',
                      'prefix' => 'cake_model_',
                      'serialize' => true
                  ]
              ]);

\Cake\Datasource\ConnectionManager::config('test', [
    'className' => 'Cake\Database\Connection',
    'driver' => 'Cake\Database\Driver\Sqlite',
    'database' => ':memory:'
]);

require 'config/bootstrap.php';
