<?php

namespace BBCode\Test\TestCase\Lib;

use BBCode\Lib\BBCodeParser;
use BBCode\Lib\TagData;
use Cake\Core\Configure;
use Cake\TestSuite\TestCase;
use Cake\View\Cell;

class TestTagRenderer extends Cell {

    public function tag (TagData $data) {
        $this->set(compact('data'));
        $this->template = 'test';
        $this->viewBuilder()->templatePath('Lib');
    }

    /**
     * @param TagData $data the tag data
     * @standalone
     */
    public function standalone (TagData $data) {
        $this->tag($data);
    }

    public function _list (TagData $data) {
        $this->tag($data);
    }

    public function _parent (TagData $data) {
        $this->set('content', $data->content);
        $this->viewBuilder()->templatePath('Lib');
    }

}

class BBCodeParserTest extends TestCase {

    /**
     * @var BBCodeParser
     */
    private $parser;

    public function setUp () {
        parent::setUp();
        Configure::write('BBCode.renderCell', 'BBCode\Test\TestCase\Lib\TestTagRenderer');
        Configure::write('debug', true);
        $this->parser = new BBCodeParser(true);
    }

    public function testOnlySpace () {
        $html = $this->parser->parse('    ');
        $this->assertEquals('    ', $html);
    }

    public function testNoTag () {
        $bbCode = 'Lorem ipsum';
        $html = $this->parser->parse($bbCode);
        $this->assertEquals($bbCode, $html);
    }

    public function testTagRenderCall () {
        $bbCode = '[tag][/tag]';
        $html = $this->parser->parse($bbCode);
        $expected = ['tagName' => 'tag', 'parent' => '', 'content' => '', 'attributes' => []];
        $this->assertEquals($expected, json_decode($html, true));
    }

    /**
     * @depends testTagRenderCall
     */
    public function testTagRenderCallUnderscored () {
        $bbCode = '[list][/list]';
        $html = $this->parser->parse($bbCode);
        $expected = ['tagName' => 'list', 'parent' => '', 'content' => '', 'attributes' => []];
        $this->assertEquals($expected, json_decode($html, true));
    }

    /**
     * @depends testTagRenderCall
     */
    public function testTagContent () {
        $bbCode = '[tag]test[/tag]';
        $html = $this->parser->parse($bbCode);
        $expected = ['tagName' => 'tag', 'parent' => '', 'content' => 'test', 'attributes' => []];
        $this->assertEquals($expected, json_decode($html, true));
    }

    /**
     * @depends testTagRenderCall
     */
    public function testStandaloneTag () {
        $bbCode = '[tag/]';
        $html = $this->parser->parse($bbCode);
        $expected = ['tagName' => 'tag', 'parent' => '', 'content' => '', 'attributes' => []];
        $this->assertEquals($expected, json_decode($html, true));
    }

    /**
     * @depends      testTagRenderCall
     * @dataProvider standaloneTags
     * @param string $bbCode the bbCode to test
     * @param array $expected the expected tag data
     */
    public function testShortStandaloneTag ($bbCode, $expected) {
        $html = $this->parser->parse($bbCode);
        $this->assertEquals($expected, json_decode($html, true));
    }

    public function standaloneTags () {
        return [
            'toplevel' => ['[standalone]', ['tagName' => 'standalone', 'parent' => '', 'content' => '', 'attributes' => []]],
            'nested' => ['[parent][standalone][/parent]', ['tagName' => 'standalone', 'parent' => 'parent', 'content' => '', 'attributes' => []]]
        ];
    }

    public function testStandaloneWithContent () {
        $this->setExpectedException('\BBCode\Exception\ContentNotAllowedException', 'standalone tag [standalone] is not allowed to have content');
        $bbCode = '[standalone]test[/standalone]';
        $html = $this->parser->parse($bbCode);
    }

    /**
     * @depends testTagContent
     * @depends testShortStandaloneTag
     * @dataProvider parentTags
     * @param string $bbCode the bbCode to test
     * @param string $expected the expected parsed result
     */
    public function testTagParent ($bbCode, $expected) {
        $html = $this->parser->parse($bbCode);
        $this->assertEquals($expected, $html);
    }

    public function parentTags () {
        return [
            'simple parent' => ['[parent][tag][/tag][/parent]', json_encode(['tagName' => 'tag', 'attributes' => (object) [], 'content' => '', 'parent' => 'parent'])],
            'parent with standalone tag' => ['[parent][standalone][tag][/tag][/parent]',
                                             json_encode(['tagName' => 'standalone', 'attributes' => (object) [], 'content' => '', 'parent' => 'parent']) .
                                             json_encode(['tagName' => 'tag', 'attributes' => (object) [], 'content' => '', 'parent' => 'parent'])]
        ];
    }

    /**
     * @depends testTagParent
     * @depends testStandaloneTag
     * @depends testShortStandaloneTag
     * @dataProvider tagsInText
     * @param string $bbCode the bbCode to test
     * @param string $expected the expected parsed result
     */
    public function testTagInText ($bbCode, $expected) {
        $html = $this->parser->parse($bbCode);
        $this->assertEquals($expected, $html);
    }

    public function tagsInText () {
        $standalone = json_encode(['tagName' => 'standalone', 'attributes' => (object) [], 'content' => '', 'parent' => '']);
        return [
            'single tag' => ['test[tag][/tag]test', 'test' . json_encode(['tagName' => 'tag', 'attributes' => (object) [], 'content' => '', 'parent' => '']) . 'test'],
            'parent tag' => ['test[parent]test[tag][/tag]test[/parent]test',
                             'testtest' . json_encode(['tagName' => 'tag', 'attributes' => (object) [], 'content' => '', 'parent' => 'parent']) . 'testtest'],
            'standalone tag' => ['test[tag/]test', 'test' . json_encode(['tagName' => 'tag', 'attributes' => (object) [], 'content' => '', 'parent' => '']) . 'test'],
            'short standalone tag' => ['test[standalone]test',
                                       'test' . $standalone . 'test'],
            'multiple standalone tags' => ['test[standalone]test[standalone]test',
                                           'test' . $standalone . 'test' . $standalone . 'test'],
            'nested standalone tag' => ['test[parent]test[standalone]test[/parent]test',
                             'testtest' . json_encode(['tagName' => 'standalone', 'attributes' => (object) [], 'content' => '', 'parent' => 'parent']) . 'testtest']
        ];
    }

    /**
     * @depends testTagRenderCall
     */
    public function testTagAttribute () {
        $bbCode = '[tag test="[]<test" test2][/tag]';
        $html = $this->parser->parse($bbCode);
        $expected = ['tagName' => 'tag', 'parent' => '', 'content' => '', 'attributes' => ['test' => '[]<test', 'test2' => 'test2']];
        $this->assertEquals($expected, json_decode($html, true));
    }

    /**
     * @dataProvider illegalTagNames
     * @param string $tagName the tagName to test
     */
    public function testIllegalTagName ($tagName) {
        $this->setExpectedException('\BBCode\Exception\InvalidTagNameException', sprintf("Tagname '%s' is invalid", h($tagName)));
        $bbCode = sprintf('[%1$s][/%1$s]', $tagName);
        $this->parser->parse($bbCode);
    }

    public function illegalTagNames () {
        $illegalChars = '=&<>{}|!$%?+*~#\',:;@`^';
        $tags = ['0tag' => ['0tag']];
        foreach (str_split($illegalChars) as $char) {
            $tag = sprintf('t%sg', $char);
            $tags[$tag] = [$tag];
        }
        return $tags;
    }

    public function testNoSuchTag () {
        $this->setExpectedException('\BadMethodCallException', 'test');
        $bbCode = '[test][/test]';
        $this->parser->parse($bbCode);
    }

    /**
     * @depends testNoSuchTag
     */
    public function testExceptionTranslation () {
        Configure::write('debug', false);
        $this->setExpectedException('\BBCode\Exception\UnknownTagException', '[test]');
        $bbCode = '[test][/test]';
        $this->parser->parse($bbCode);
    }

    public function testTagNameMismatch () {
        $this->setExpectedException('\BBCode\Exception\UnexpectedStringException', 'Expected [/test], got [/tag]');
        $bbCode = '[test][/tag]';
        $this->parser->parse($bbCode);
    }

    /**
     * @param string $attribute the attribute definition to test
     * @dataProvider invalidAttributeFormats
     */
    public function testInvalidAttributeFormat ($attribute) {
        $this->setExpectedException('\BBCode\Exception\AttributeFormatException', sprintf("Invalid attribute definition '%s' in tag [test]", h($attribute)));
        $bbCode = sprintf('[test %s /]', $attribute);
        $this->parser->parse($bbCode);
    }

    public function invalidAttributeFormats () {
        $tags = $this->illegalTagNames();
        $cases = [];
        foreach ($tags as $key => list($tag)) {
            $cases['invalid attribute name:' . $key] = [sprintf('%s="value"', $tag)];
        }
        return [
            'single quotes' => ["key='value'"],
            'no quotes' => ['key=value']
        ] + $cases;
    }

    /**
     * @dataProvider slashTags
     * @param string $bbCode the BBCode to test
     */
    public function testTwoSlashes ($bbCode) {
        $this->setExpectedException('\BBCode\Exception\UnexpectedStringException', "Unexpected '/'");
        $this->parser->parse($bbCode);
    }

    public function slashTags () {
        return [
            'endTag' => ['[test][/te/st]'],
            'standaloneTag' => ['[test//]']
        ];
    }

    public function testMissingStartTag () {
        $this->setExpectedException('\BBCode\Exception\UnexpectedStringException', "Unexpected END-TAG");
        $this->parser->parse('[/test]');
    }

    public function testUnexpectedSquareBracket () {
        $this->setExpectedException('\BBCode\Exception\UnexpectedStringException', "Unexpected '['");
        $this->parser->parse('[te[st]');
    }

    public function testIncompleteTag () {
        $this->setExpectedException('\BBCode\Exception\UnexpectedStringException', "Expected ']', got END_OF_STRING");
        $this->parser->parse('[test');
    }

    public function testMissingEndTag () {
        $this->setExpectedException('\BBCode\Exception\UnexpectedStringException', "Expected [/test], got END_OF_STRING");
        $this->parser->parse('[test]');
    }

}
